import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from './services/http.service';
import { StoreService } from './services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private filterSubscription: Subscription | null;

  constructor(
    private httpService: HttpService,
    public store: StoreService
  ){
    this.filterSubscription = null
  }

  ngOnInit(){


    this.filterSubscription = this.httpService.getFilterData().subscribe((data:any) => {

    });

  }

  ngOnDestroy() {
    //this.filterSubscription.unsubscribe()
  }

}
