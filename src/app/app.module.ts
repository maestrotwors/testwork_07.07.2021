import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule }   from '@angular/common/http';

//Material
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';

//Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';

import { ScreenFilterMainComponent } from './components/screens/screen-filter-main/screen-filter-main.component';
import { ScreenSearchByTitleComponent } from './components/screens/screen-search-by-title/screen-search-by-title.component';
import { ScreenSearchCategoryComponent } from './components/screens/screen-search-category/screen-search-category.component';

import { AppInputComponent } from './components/shared/app-input/app-input.component';
import { AppChipComponent } from './components/shared/app-chip/app-chip.component';
import { AppExpansionPanelComponent } from './components/shared/app-expansion-panel/app-expansion-panel.component';
import { AppInputAutocompleteComponent } from './components/shared/app-input-autocomplete/app-input-autocomplete.component';
import { AppSlideToggleComponent } from './components/shared/app-slide-toggle/app-slide-toggle.component';

//Services
import { HttpService } from './services/http.service';
import { StoreService } from './services/store.service';

const materialModules = [
  MatIconModule
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,

    ScreenFilterMainComponent,
    ScreenSearchByTitleComponent,
    ScreenSearchCategoryComponent,

    AppInputComponent,
    AppChipComponent,
    AppExpansionPanelComponent,
    AppInputAutocompleteComponent,
    AppSlideToggleComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatDividerModule,
    MatChipsModule,
    MatInputModule,
    MatExpansionModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    MatRadioModule,

    ...materialModules
  ],
  exports:[
    ...materialModules
  ],
  providers: [ HttpService, StoreService ],
  bootstrap: [ AppComponent ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
