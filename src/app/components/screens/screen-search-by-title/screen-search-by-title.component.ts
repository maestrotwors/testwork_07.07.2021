import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../../services/store.service';

@Component({
  selector: 'app-screen-search-by-title',
  templateUrl: './screen-search-by-title.component.html',
  styleUrls: ['./screen-search-by-title.component.scss']
})
export class ScreenSearchByTitleComponent implements OnInit {

  constructor(public store: StoreService) { }

  ngOnInit(): void {
  }

}
