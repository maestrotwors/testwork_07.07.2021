import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../../services/store.service';

@Component({
  selector: 'app-screen-search-category',
  templateUrl: './screen-search-category.component.html',
  styleUrls: ['./screen-search-category.component.scss']
})
export class ScreenSearchCategoryComponent implements OnInit {

  checkbox = true;
  constructor(public store: StoreService) { }

  ngOnInit(): void {
  }

  removeCategory(id: number): void {
    alert("Category removed");
  }
}
