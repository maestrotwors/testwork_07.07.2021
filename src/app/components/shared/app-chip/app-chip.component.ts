import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-chip',
  templateUrl: './app-chip.component.html',
  styleUrls: ['./app-chip.component.scss']
})
export class AppChipComponent{
  @Input() title: string = "";
}
