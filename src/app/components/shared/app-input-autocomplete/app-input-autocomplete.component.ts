import { Component, OnInit, Input } from '@angular/core';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-input-autocomplete',
  templateUrl: './app-input-autocomplete.component.html',
  styleUrls: ['./app-input-autocomplete.component.scss']
})
export class AppInputAutocompleteComponent implements OnInit {
  @Input() data =[""];
  @Input() iconPrefix: string = "";
  @Input() placeholder: string = "";

  myControl = new FormControl();
  //options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]> | null = null;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string){
    const filterValue = value.toLowerCase();
    return this.data.filter(option => option.toLowerCase().includes(filterValue));
  }
}
