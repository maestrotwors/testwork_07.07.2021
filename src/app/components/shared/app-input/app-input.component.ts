import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './app-input.component.html',
  styleUrls: ['./app-input.component.scss']
})
export class AppInputComponent {
  @Input() iconPrefix: string = "";
  @Input() placeholder: string = "";
  @Input('width') width: ('small' | 'large') = 'large';
  constructor() { }

}
