import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HttpService {

  constructor(private http_client: HttpClient) {}

  public getFilterData(){
    return this.http_client.get('https://api.npoint.io/b45f5662daab7f1b7fd4');
  }

}
