import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StoreService {

  constructor() {
    this.categories = [{"id": 1, "name":"Accounting"},{"id": 2, "name":"Arts"},{"id": 3, "name":"Healthcare"}]
    this.categoriesFilter = "";
    this.searchByTitle = "";
    this.categoriesChildren = new Array<object>();
    this.searchByTitleArray =  ["Java Developer", "Angular Developer", "President of country"];
  }

  public categories: Array<object>;
  public categoriesFilter;
  public searchByTitle;
  public categoriesChildren: Array<object>;

  //-------Временно забитые данные
  public searchByTitleArray: string[];


}
